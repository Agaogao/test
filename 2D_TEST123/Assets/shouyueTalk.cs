﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class shouyueTalk : MonoBehaviour {

	public GameObject canvas;

	//private int sentenses;

	private int currentsentense;

	public AudioSource music;
	public float musicVolume;



	// Use this for initialization
	void Start () {
		canvas.SetActive (false);
		//sentenses = 3;
		currentsentense = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	void OnTriggerStay(Collider collider)
	{
		if (collider.tag.CompareTo("Player") == 0)
		{
			if (Input.GetKeyDown (KeyCode.F))
			{
				canvas.SetActive (true);
				GameObject.Find("Dialogue/Text001").GetComponent<Text>().text = "Hello";
				Image icon = GameObject.Find("Dialogue/Image001").GetComponent<Image>();
				Sprite sp = Resources.Load("shouyuehead", typeof(Sprite)) as Sprite;
				icon.sprite = sp;
				currentsentense = 1;
				musicVolume = music.volume;
				music.volume = 0.2F;

			}

			if(Input.GetMouseButtonDown(0))
			{
				if(currentsentense == 1)
				{
					GameObject.Find("Dialogue/Text001").GetComponent<Text>().text = "How are you?";
					currentsentense++;
				}
				else if(currentsentense == 2)
				{
					GameObject.Find("Dialogue/Text001").GetComponent<Text>().text = "I'm fine thank you.And you?";
					currentsentense++;
				}
				else if(currentsentense == 3)
				{
					music.volume = musicVolume;
					canvas.SetActive (false);
				}

			}
		}


	}
}
